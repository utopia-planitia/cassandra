apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    release: kube-prometheus-stack
  name: alerting-rules
spec:
  groups:
    - name: ./cassandra-rules.rules
      rules:
        - alert: CassandraOneNodeLost
          expr: kube_statefulset_status_replicas{namespace="cassandra",statefulset="k8ssandra-dc1-default-sts"} - kube_statefulset_status_replicas_ready{namespace="cassandra",statefulset="k8ssandra-dc1-default-sts"} == 1
          for: 2m
          labels:
            severity: warning
          annotations:
            summary: "There is one instance down on Cassandra cluster."
            description: "There is one instance down on Cassandra cluster."
        - alert: CassandraTwoOrMoreNodesLost
          expr: kube_statefulset_status_replicas{namespace="cassandra",statefulset="k8ssandra-dc1-default-sts"} - kube_statefulset_status_replicas_ready{namespace="cassandra",statefulset="k8ssandra-dc1-default-sts"} > 1
          for: 2m
          labels:
            severity: critical
          annotations:
            summary: "There are two or more nodes down on Cassandra cluster."
            description: "There are two or more nodes down on Cassandra cluster."
        - alert: CassandraPodMemoryWarning
          expr: max(container_memory_usage_bytes{namespace="cassandra",container="cassandra"}) by (pod) / min(cluster:namespace:pod_memory:active:kube_pod_container_resource_limits{namespace="cassandra", container="cassandra"}) by (pod) * 100 > 65
          for: 1m
          labels:
            severity: warning
          annotations:
            summary: "Cassandra memory limit warning."
            description: "Cassandra memory reached {{ `{{$value}}` }}% on cassandra. You need to upgrade its memory to avoid Cassandra pod issues."
        - alert: CassandraPodMemoryCritical
          expr: max(container_memory_usage_bytes{namespace="cassandra",container="cassandra"}) by (pod) / min(cluster:namespace:pod_memory:active:kube_pod_container_resource_limits{namespace="cassandra", container="cassandra"}) by (pod) * 100 > 80
          for: 1m
          labels:
            severity: critical
          annotations:
            summary: "Cassandra memory limit critical."
            description: "Cassandra memory reached {{ `{{$value}}` }}% on cassandra. You need to upgrade its memory as soon as possible to avoid Cassandra OOM."
        - alert: CassandraJmxMemoryWarning
          expr: mcac_jvm_memory_usage{namespace="cassandra",container="cassandra",memory_type="heap"} * 100 > 85
          for: 10m
          labels:
            severity: warning
          annotations:
            summary: "Cassandra JMX memory usage warning."
            description: "Cassandra JMX memory reached {{ `{{$value}}` }}% on cassandra. You need to upgrade its memory to avoid Cassandra pod issues."
        - alert: CassandraJmxMemoryCritical
          expr: mcac_jvm_memory_usage{namespace="cassandra",container="cassandra",memory_type="heap"} * 100 > 90
          for: 10m
          labels:
            severity: critical
          annotations:
            summary: "Cassandra JMX memory usage critical."
            description: "Cassandra JMX memory reached {{ `{{$value}}` }}% on cassandra. You need to upgrade its memory as soon as possible to avoid Cassandra OOM."
