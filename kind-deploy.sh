#!/bin/sh

set -eux

KIND_NAME=cassandra
readonly KIND_NAME
KIND_CONTEXT="kind-${KIND_NAME:?}"
readonly KIND_CONTEXT

# update the CRDs
if test -x ./ci/post-update.d/k8ssandra-operator-crds.sh; then
  ./ci/post-update.d/k8ssandra-operator-crds.sh
fi

# create the kind cluster if it does not exist yet
kind create cluster --name "${KIND_NAME:?}" || true

# install fully functional cert-manager
kubectl --context="${KIND_CONTEXT:?}" apply --filename=https://github.com/cert-manager/cert-manager/releases/download/v1.10.1/cert-manager.yaml
kubectl --context="${KIND_CONTEXT:?}" rollout status deployment.apps/cert-manager-webhook --namespace=cert-manager --watch=true --timeout=300s

# install prometheus CRDs
kubectl --context="${KIND_CONTEXT:?}" apply --filename=https://github.com/prometheus-operator/prometheus-operator/releases/download/v0.61.1/stripped-down-crds.yaml

# install k8ssandra CRDs
helmfile \
  --kube-context="${KIND_CONTEXT:?}" \
  --file=k8ssandra-operator-crds/helmfile.yaml \
  --state-values-file=./../ci/cluster.yaml \
  sync

# install k8ssandra-operator
helmfile \
  --kube-context="${KIND_CONTEXT:?}" \
  --selector=phase=init \
  --file=k8ssandra-operator/helmfile.yaml \
  --state-values-file=./../ci/cluster.yaml \
  sync

# wait for the k8ssandra-operator and the cass-operator
kubectl --context="${KIND_CONTEXT:?}" rollout status deployment.apps/k8ssandra-operator --namespace=cassandra --watch=true --timeout=300s
kubectl --context="${KIND_CONTEXT:?}" rollout status deployment.apps/k8ssandra-operator-cass-operator --namespace=cassandra --watch=true --timeout=300s

# install the k8ssandra cluster (adjust datacenter size and storageClassName to fit into a single-node kind cluster)
helmfile \
  --kube-context="${KIND_CONTEXT:?}" \
  --selector=phase!=init \
  --file=k8ssandra-operator/helmfile.yaml \
  --state-values-file=./../ci/cluster.yaml \
  sync \
  --set=cassandra.datacenter.size=1 \
  --set=cassandra.datacenter.storageConfig.cassandraDataVolumeClaimSpec.storageClassName=null

# wait until the cassandra stateful set exists and is rolled out
while ! kubectl --context="${KIND_CONTEXT:?}" rollout status statefulset.apps/k8ssandra-dc1-default-sts --namespace=cassandra --watch=true --timeout=300s; do
  sleep 5
done

# wait until the reaper deployment exists and is rolled out 
while ! kubectl --context="${KIND_CONTEXT:?}" rollout status deployment.apps/k8ssandra-dc1-reaper --namespace=cassandra --watch=true --timeout=300s; do
  sleep 5
done

# prepare the tests
helmfile \
  --kube-context="${KIND_CONTEXT:?}" \
  --file=tests/helmfile.yaml \
  --state-values-file=./../ci/cluster.yaml \
  sync

# run the tests
helmfile \
  --kube-context="${KIND_CONTEXT:?}" \
  --file=tests/helmfile.yaml \
  --state-values-file=./../ci/cluster.yaml \
  test --logs
