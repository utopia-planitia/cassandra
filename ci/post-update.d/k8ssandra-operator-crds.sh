#!/usr/bin/env bash

set -euo pipefail

# create a temporary directory to be used as output directory of "helmfile template"
TMP_DIR="$(mktemp -d)"
readonly TMP_DIR
trap 'rm -fr "${TMP_DIR:?}"' EXIT

# render the templates into the temporary directory
helmfile \
    --file k8ssandra-operator/helmfile.yaml \
    --selector=name=k8ssandra-operator \
    --state-values-file=../ci/cluster.yaml \
    template \
    --include-crds \
    --skip-tests \
    --output-dir="${TMP_DIR:?}" \
    --output-dir-template='{{ .OutputDir }}'

# use chart-prettier to give the files a consistent naming
chart-prettier --stdin=false "${TMP_DIR:?}"/k8ssandra-operator/crds

OUT_DIR=k8ssandra-operator-crds/chart/templates
readonly OUT_DIR

rm -fr "${OUT_DIR:?}"
mkdir -p "${OUT_DIR:?}"

cp "${TMP_DIR:?}"/k8ssandra-operator/crds/customresourcedefinition-*.yaml "${OUT_DIR:?}"
