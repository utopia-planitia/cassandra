# Cassandra

A deployment of [K8ssandra](https://k8ssandra.io/) with documentation at https://docs.k8ssandra.io/.

## Usage

[Retrieve K8ssandra superuser credentials](https://docs-v1.k8ssandra.io/install/local/#superuser)

## Monitoring

Dashboards in Grafana and Alerting Rules for Prometheus.

The UI of reaper is accessable via `https://reaper.{cluster-domain}/webui`
